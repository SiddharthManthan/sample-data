##### To restore
`cp -r public/upload/* /var/www/public/socialbook/upload/`
`docker cp SOCIAL_SAMPLE_DATA.sql mysql:\data.sql`
`docker exec -it mysql /bin/bash`
`mysql -udbuser -p`
`use social`
`source /data.sql`